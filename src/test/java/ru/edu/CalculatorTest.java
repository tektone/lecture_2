package ru.edu;

import org.junit.Assert;
import org.junit.Test;

public class CalculatorTest {

    private Calculator calc = new Calculator();

    @Test
    public void sumPositiveDigits() {

        Assert.assertEquals(10, calc.sum(5, 5));
        Assert.assertEquals(5, calc.sum(5, 0));
        Assert.assertEquals(5, calc.sum(0, 5));
    }

    @Test
    public void sumNegativeDigits() {

        Assert.assertEquals(0, calc.sum(5, -5));
        Assert.assertEquals(-5, calc.sum(-5, 0));
        Assert.assertEquals(-5, calc.sum(0, -5));

        Assert.assertEquals(1, calc.sum(6, -5));
    }

    @Test
    public void divTest() {

        Assert.assertEquals(5, calc.div(25, 5));
        Assert.assertEquals(2, calc.div(5, 2));
        Assert.assertEquals(4, calc.div(8, 2));
    }

    @Test
    public void exceptionTest() {

        try {
            calc.div(2, 0);
            Assert.fail();
        } catch (IllegalArgumentException ex) {
            System.out.println("Illegal Arg exc = " + ex.toString());
        }
    }
}
