package ru.edu;

import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AppTests {

    @Test
    public void myTest() {

        int a = 0;
        int b = 1;

        Assert.assertEquals(3, 2 + 1);

    }

    @Test
    public void myStringTest() {

        String a = "Hello ";
        String b = new String("World");

        Assert.assertEquals("Hello World", a + b);

    }

    @Test
    public void exceptionTst() {
        List<Integer> list = new ArrayList<>();

        Assert.assertTrue(list.isEmpty());
        Assert.assertEquals(0, list.size());

        list.add(10);
        System.out.println(list);

        Assert.assertFalse(list.isEmpty());
        Assert.assertEquals(1, list.size());

        Assert.assertEquals(Arrays.asList(10), list);

        try {
            list.get(10);
            Assert.fail();
        } catch (IndexOutOfBoundsException e) {
            System.out.println("exception returned");
        }
    }
}
