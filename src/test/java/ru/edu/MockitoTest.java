package ru.edu;

import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

public class MockitoTest {

    @Test
    public void test(){
        List <String> listMock = Mockito.mock(List.class);

        Mockito.when(listMock.get(Mockito.anyInt())).thenReturn("AnyNumb");


        System.out.println("isEmpty: " + listMock.isEmpty());
        listMock.add("1");
        listMock.add("2");
        System.out.println("List.get(0): " + listMock.get(0));
        System.out.println("List.get(1): " + listMock.get(1));
    }
}
