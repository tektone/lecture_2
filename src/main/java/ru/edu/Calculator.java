package ru.edu;

public class Calculator {

    public int sum(int a, int b) {
        return a + b;
    }

    public int div(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("Не тупи. " +a + " На " + b + " ноль делить нельзя");
        }
        return a / b;
    }

}
